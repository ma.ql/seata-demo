package com.qjc.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @ClassName: StorageService
 * @Description: 库存服务：扣减库存
 * @Author: qiaojiacheng
 * @Date: 2021/4/13 4:45 下午
 */
@FeignClient(value = "storageService", url = "http://127.0.0.1:18082")
@Service
public interface StorageService {

    @RequestMapping(path = "/storage/{commodityCode}", method = RequestMethod.GET)
    String storage(@RequestParam("commodityCode") String commodityCode);

}
