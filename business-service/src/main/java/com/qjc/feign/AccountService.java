package com.qjc.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @ClassName: AccountService
 * @Description: 账户服务：扣钱
 * @Author: qiaojiacheng
 * @Date: 2021/4/13 4:35 下午
 */
@FeignClient(value = "accountService", url = "http://127.0.0.1:18084")
@Service
public interface AccountService {

    @RequestMapping(path = "/account", method = RequestMethod.GET)
    String updateMoney(@RequestParam("userId") String userId, @RequestParam("money") int money);

}
